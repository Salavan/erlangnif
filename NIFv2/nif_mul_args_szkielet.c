/* nif_mul_args_szkielet.c */
#include "erl_nif.h"
#include <stdlib.h>



static ERL_NIF_TERM nif_mul_args_fun(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])   
{       
       int len;
       if (!enif_get_list_lenght(env, argv[0], &len)             // w argv[0] lista argument�w wczytywana do tablicy i przekazywana do funkcji
          {
           return enif_make_badarg(env);                            
          }
       else {
            int *tab;
            tab = (int*) malloc(len*sizeof(int));
            
            ERL_NIF_TERM h, t;
            t = argv[0];
            int i, el;
            for (i=0, i<len, i++){                                  //pobierz, konwertuj i wstaw do tablicy ka�dy element (int)
                if( enif_get_list_cell(env, t, &h, &t))
                if(!enif_get_int(env, h, &el ) ) return  enif_make_badarg(env); 
                tab[i] = el; 
                }
            int ret = mul_args_fun(tab, len) ;                      //wywo�aj funkcj� z tablic� i liczb� element�w w tablicy
             return enif_make_int(env, ret);
            }
}

int mul_args_fun(int * tab, int len){
               // cia�o wywo�ywanej funkcji
    return 0;
    }


static ErlNifFunc nif_funcs[] = {
    {"mul_args_fun", 1, nif_mul_args_fun}             
     /* 
     {"<nazwa funkcji w erlangu>", <lb. argumentow>, <nazwa wywo�ywanej funkcji>},
     { , , } ,
     { , , } ,
     ...
     */
};

ERL_NIF_INIT(nazwaModuluErlangowego, nif_funcs, NULL, NULL, NULL, NULL)       //nazwa modu�u erlangowego, kt�ry korzysta z biblioteki NIF�w



