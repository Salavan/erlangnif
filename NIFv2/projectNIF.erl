-module(projectNIF).

-export([nif_hard_calc/3, init/0, hard_calc/3, benchmark/3]).
-on_load(init/0).

init() -> erlang:load_nif("./niftest", 0).

hard_calc(X,Y,Z) -> hard_calc2(X,Y,Z,100000000).

hard_calc2(X,Y,Z,T) when T>0 ->
	(X*Y*Z)/(X+Y+Z),
	hard_calc2(X,Y,Z,T-1);
hard_calc2(_,_,_,_) -> done.

nif_hard_calc(X, Y, Z) -> {"NIF library could not be loaded.", X, Y, Z}.

benchmark(X, Y, Z) -> {T1, _} = timer:tc(?MODULE, hard_calc,[X,Y,Z]),
						{T2, _} = timer:tc(?MODULE, nif_hard_calc,[X,Y,Z]),
						io:format("Czas wykonania 100 mln operacji arytmetycznych w Erlangu: ~B microseconds ~nCzas wykonania 1000 mln operacji artymetycznych przez NIFa: ~B microseconds~n~n", [T1, T2]),
						io:format("Z czego mozemy odczytac, ze proste operacje arytmetyczne wykonywane przez NIF-a~nwykonuja sie ~f razy szybciej.~n", [T1*10 / T2]),
						ok.
						