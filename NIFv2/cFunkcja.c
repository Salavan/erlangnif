#include "erl_nif.h"

extern int nif_hard_calc(int x, int y, int z);



static ERL_NIF_TERM function_c_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int x,y,z, ret;

    if (!enif_get_int(env, argv[0], &x)) {
	return enif_make_badarg(env);
    }

    if (!enif_get_int(env, argv[1], &y)) {
	return enif_make_badarg(env);
    }

    if (!enif_get_int(env, argv[2], &z)) {
	return enif_make_badarg(env);
    }

    ret = nif_hard_calc(x,y,z);
    return enif_make_int(env, ret);
}


static ErlNifFunc nif_funcs[] = {
    {"nif_hard_calc", 3, function_c_nif}
};

ERL_NIF_INIT(projectNIF, nif_funcs, NULL, NULL, NULL, NULL)
